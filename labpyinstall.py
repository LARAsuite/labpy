#!/usr/bin/env python3
"""_____________________________________________________________________

:PROJECT: labPy

*labPy  installer script*

:details: This is the labPy installer script.
          Installing core labPy library
          and optional tools interactivly.
          
:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20180930
________________________________________________________________________
"""
import sys
import os

import simpleinstall as si

HOME_DIR = os.environ.get('HOME')
REPO_DIR = os.path.dirname(os.path.realpath(__file__)) # current directory of this repository

# Display Welcome Text
installer_welcome_txt = ("______________________________________________\n\n"
                         "This is the labPy installer script\n"
                         "It will guide you through the complete installation \n"
                         "of the core parts of labPy ...\n"
                         "[type: ?    - for further information\n"
                         "       help - to list all input options    ]\n"
                         "______________________________________________\n")

print(installer_welcome_txt)

# Install virtual Python3 environment ?
inst_venv = si.query_yes_no("Install a virtual Python environment (recommended) ?", 
                          help="HELP: This is the recommended installation mode for testing labPy")
if inst_venv:
    venv_name = "labpy_venv"
    venv_dir = si.query("Please specify a directory for your virtual python3 environment", 
                     default_answer=os.path.join(HOME_DIR,"python3",venv_name), 
                     help="HELP: specify the target directory for the virtual python3 environment")
    
    create_venv_anyway = True
   
    if os.path.exists(venv_dir): 
        create_venv_anyway = si.query_yes_no("\nWARNING !! Virtual environment exists: [{}], shall I create it anyway ?".
                                          format(venv_dir), default_answer='no', 
                                          help="HELP: create the python 3 virtual environment anyway ?")
    
    if create_venv_anyway:
        try: # should be replaced by importAndInstall, when this is working
            import venv
        except ImportError:
            import pip
            pip.main(['install', 'venv'])
            import venv
        #installAndImport('venv') # making sure, that venv package is available
        
        print("\t...creating venv in dir [{}]".format(venv_dir))
        if venv is not None:
            venv.create(venv_dir, system_site_packages=False, clear=False, symlinks=False, with_pip=True)
        
    print("* Activating venv [{}]".format(venv_dir)) # this is done by prepending python3 path to sytem path
    os.environ['PATH'] = os.pathsep.join( [ os.path.join(venv_dir, 'bin'), os.environ['PATH'] ] )
    
# Install dependancies ? - pip3 is faster than dependancies from setup.py
if si.query_yes_no("Install library dependancies (recommended) ?", 
                 help="HELP: This will install all required packages"):
    print("Installing dependancies ...")
    
    requirements = os.path.join(REPO_DIR, 'requirements', 'requirements_base.txt')
    
    if inst_venv:
        si.call('pip3 install -r ' + requirements )
    else:
        import pip
        pip.main(['install', '-r' , requirements])

# Install hardware libraries ?
# serial, CAN, SiLA2
if si.query_yes_no("Install hardware interface libraries, like serial/RS232 (recommended) ?", 
                 help="HELP: This will install interface libraries"):
    print("[not working yet] Installing interface libraries ...")
   
    #~ if inst_venv:        
        #~ si.runSetup(src_dir=os.path.join(REPO_DIR, 'sila_library'), 
                 #~ lib_dir=venv_dir)
        
else:
    sys.stdout.write("Well, what a pitty ... - you should really consider installing the hardware interface libraries \n")
    
# Generate documentation ?
if si.query_yes_no("Generate documentation (recommended) ?", 
                 help="HELP: This will generate the documentation"):
    print("[not working yet] : Generating documentation ... ")

if inst_venv:
    print("\nAttention: Please do not forget to activate the virtual environment by calling: \n\n"
           "source {}/bin/activate \n\n"
           "- to deactivate the venv, simply type:\n\n"
           "deactivate \n\n"
           "Enjoy the labPy with Python3 :)\n".format(venv_dir) )
